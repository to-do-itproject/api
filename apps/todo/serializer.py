from rest_framework import serializers

from apps.todo.models import ToDo
from apps.shared.serializer import BaseModelSerializerMeta as Meta


class ToDoSerializer(serializers.ModelSerializer):
    class Meta(Meta):
        model = ToDo
        fields = "__all__"
        read_only_fields = Meta.read_only_fields + ('checked',)
