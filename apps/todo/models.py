from django.db import models

from apps.shared.models import BaseModel
from apps.shared.models.base import DEFAULT_PERMISSIONS


class ToDo(BaseModel):
    title = models.CharField(max_length=64)
    description = models.TextField()
    checked = models.BooleanField(default=False)

    class Meta(BaseModel.Meta):
        default_permissions = DEFAULT_PERMISSIONS + (
            'apply',
        )

    def __str__(self):
        return self.title

    def apply(self):
        self.checked = True
        self.save()
