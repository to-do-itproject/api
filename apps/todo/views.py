from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.todo.models import ToDo
from apps.todo.serializer import ToDoSerializer


class ToDoViewSet(viewsets.ModelViewSet):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer
    search_fields = ('title', )

    @action(['post'], url_path='check', detail=True)
    def check(self, request, pk):
        todo = self.get_object()
        todo.apply()
        data = ToDoSerializer(todo).data
        return Response(data=data, status=status.HTTP_200_OK)
