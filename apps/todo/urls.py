from django.urls import path, include
from rest_framework import routers

from apps.todo.views import ToDoViewSet

app_name = 'apps.todo'

router = routers.DefaultRouter()
router.register(r'todo', ToDoViewSet, basename='todo')

urlpatterns = [
    path('', include(router.urls)),
]
