from rest_framework import serializers


class ListIdSerializer(serializers.Serializer):
    ids = serializers.ListField(
        child=serializers.IntegerField(min_value=1),
        required=True
    )


class BaseModelSerializerMeta:
    read_only_fields = ('created_date', 'modified_date',)
    fields = '__all__'
