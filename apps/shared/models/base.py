from django.db import models

DEFAULT_PERMISSIONS = (
    'list', 'retrieve', 'create', 'update', 'destroy', 'destroy_bulk'
)


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    objects = models.Manager()

    class Meta:
        abstract = True
        ordering = ('-id',)
        default_permissions = DEFAULT_PERMISSIONS
