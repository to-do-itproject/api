from django.contrib.auth.models import Group
from rest_framework import serializers

from apps.cauth.models import User
from .permission import PermissionSerializer


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name', 'permissions',)

    def to_representation(self, instance):
        self.fields['permissions'] = PermissionSerializer(many=True)
        return super().to_representation(instance)

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        users = User.objects.filter(groups=instance)
        for user in users:
            user.user_permissions.set(validated_data['permissions'])
        return instance
