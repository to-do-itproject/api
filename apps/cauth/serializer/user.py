from django.contrib.auth.models import Permission, Group
from django.contrib.auth import password_validation
from django.db import transaction

from rest_framework import serializers

from apps.cauth.models import User
from .avatar import AvatarSerializer
from .group import GroupSerializer
from .permission import PermissionSerializer


class MeUserSerializer(serializers.ModelSerializer):
    groups = serializers.PrimaryKeyRelatedField(
        queryset=Group.objects.all(),
        many=True,
        required=False
    )
    user_permissions = serializers.SlugRelatedField(
        queryset=Permission.objects.all(),
        slug_field='codename',
        many=True,
        required=False
    )
    avatar = AvatarSerializer(read_only=True)

    class Meta:
        model = User
        exclude = ('password',)

    def to_representation(self, instance):
        self.fields['avatar'] = AvatarSerializer()
        return super().to_representation(instance)


class RevisionUserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        exclude = (
            'password', 'groups', 'avatar', 'user_permissions', 'date_joined',
            'is_staff'
        )

    def get_full_name(self, obj):
        return "%s %s" % (obj.first_name, obj.last_name)


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'password',
            'is_superuser', 'user_permissions', 'groups', 'avatar', 'email',
            'is_active'
        )
        read_only_fields = (
            'modified_date', 'created_date', 'is_superuser', 'date_joined',)

    def to_representation(self, instance):
        self.fields['groups'] = GroupSerializer(many=True)
        self.fields['avatar'] = AvatarSerializer()
        return super().to_representation(instance)

    @transaction.atomic
    def create(self, validated_data):
        permissions = validated_data.pop('user_permissions', None)
        groups = validated_data.pop('groups', None)
        password = validated_data.pop('password', None)

        new_user = User(username=validated_data['username'])
        new_user.last_name = validated_data['last_name']
        new_user.first_name = validated_data['first_name']
        new_user.email = validated_data.get('email', '')
        new_user.avatar = validated_data['avatar']
        new_user.is_active = validated_data.get('is_active')

        if password:
            new_user.set_password(password)

        new_user.save()

        if permissions:
            new_user.user_permissions.clear()
            new_user.user_permissions.set(permissions)
            new_user.save()

        if groups:
            new_user.groups.clear()
            new_user.groups.set(groups)

        return new_user

    @transaction.atomic
    def update(self, instance, validated_data):
        permissions = validated_data.pop('user_permissions', [])
        groups = validated_data.pop('groups', [])
        password = validated_data.pop('password', None)

        update_user = super().update(instance, validated_data)

        if password:
            update_user.set_password(password)

        update_user.user_permissions.clear()
        update_user.user_permissions.set(permissions)
        update_user.groups.set(groups)

        update_user.save()

        return update_user


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'user_permissions',
            'groups', 'avatar', 'email', 'is_active', 'is_superuser'
        )

    def to_representation(self, instance):
        self.fields['groups'] = GroupSerializer(many=True)
        self.fields['user_permissions'] = PermissionSerializer(many=True)
        self.fields['avatar'] = AvatarSerializer()
        return super().to_representation(instance)
