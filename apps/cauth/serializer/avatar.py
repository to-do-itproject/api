from rest_framework import serializers
from apps.cauth.models import Avatar
from apps.shared.serializer import BaseModelSerializerMeta


class AvatarSerializer(serializers.ModelSerializer):
    class Meta(BaseModelSerializerMeta):
        model = Avatar
