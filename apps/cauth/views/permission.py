from django.contrib.auth.models import Permission
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ViewSet

from apps.cauth.serializer import PermissionSerializer


class PermissionListView(ListAPIView, ViewSet):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    permission_classes = (IsAuthenticated,)
    search_fields = ('name', 'codename', )
    ordering_fields = ('id', 'name', )
    pagination_class = None
