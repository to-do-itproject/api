from rest_framework import renderers, viewsets, status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import action
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.cauth.serializer import MeUserSerializer, UserSerializer, \
    UserDetailSerializer
from apps.cauth.models import User


class AuthUserView(ObtainAuthToken):
    renderer_classes = (renderers.JSONRenderer, renderers.AdminRenderer)


class MeUserView(RetrieveAPIView):
    queryset = User.objects
    serializer_class = MeUserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = (
        'first_name', 'last_name', 'email', 'username', 'groups__name',)

    def get_serializer_class(self):
        serializer_class = super().get_serializer_class()
        if self.action == 'retrieve':
            serializer_class = UserDetailSerializer
        return serializer_class

    @action(['delete'], url_path='destroy-bulk', detail=False)
    def destroy_bulk(self, request):
        ids = request.data.get('ids', [])
        User.objects.filter(id__in=ids).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
