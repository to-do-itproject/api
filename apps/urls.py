from django.urls import path, include

urlpatterns = [
    path('auth/', include(('apps.cauth.urls', 'apps.cauth'), 'cauth')),
    path('todo/', include(('apps.todo.urls', 'apps.todo'), 'todo')),
]
