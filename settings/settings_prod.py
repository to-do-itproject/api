import django_heroku
from .settings import *

DEBUG = False
DEBUG_PROPAGATE_EXCEPTIONS = True

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = (
    'https://to-do-itproject-api.herokuapp.com',
    'https://to-do-itproject-ui.herokuapp.com',
    'http://localhost:8080',
)

django_heroku.settings(locals())
